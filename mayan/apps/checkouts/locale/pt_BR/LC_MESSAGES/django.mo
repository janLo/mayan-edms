��    $      <  5   \      0  ;   1     m     �     �     �     �  "   �     �            "   1     T     i     u     �  	   �  9   �     �  &   �  '        <  !   Z     |     �     �     �     �  "   �       %   "  &   H     o     �     �     �  �  �  O   8  /   �     �     �     �     �  $   	     +	     @	     \	  "   q	     �	     �	     �	     �	  	   �	  9   �	  	   *
  '   4
  $   \
     �
      �
     �
     �
     �
               6     U  6   l  (   �     �     �     �     �                         	      #          $         "         
                                                       !                                                                  Amount of time to hold the document checked out in minutes. Block new version upload Check in document Check in documents Check in/out Check out date and time Check out details for document: %s Check out document Check out document: %s Check out documents Check out expiration date and time Checked in/available Checked out Checkout expiration Checkout time and date Checkouts Do not allow new version of this document to be uploaded. Document Document "%s" checked in successfully. Document "%s" checked out successfully. Document already checked out. Document automatically checked in Document checked in Document checked out Document checkout Document checkouts Document forcefully checked in Document has not been checked out. Documents checked out Error trying to check in document; %s Error trying to check out document; %s Forcefully check in documents No User Yes Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-27 14:10-0400
PO-Revision-Date: 2016-03-21 21:08+0000
Last-Translator: Roberto Rosario
Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Quantidade de tempo para armazenar o documento com check-out em poucos minutos. Permitir  restrições imperativas do check out Documento Check in Documentos Check in  Check in/out Data e hora do Check out Confira detalhes sobre documento:%s  Documento Check out  Check out  -  documento: %s Documentos Check out Confira data e hora de expiração Checked in disponível Checked out Check Out expiração Data e hora do Check out Checkouts Não permitir nova versão deste documento a ser enviado. Documento Sucesso no checked in -  Documento "%s" Documento "%s" check-out com êxito. Documento já fez check-out.  Documento checked in automático Documento checked in Documento checked out Documento checked out Documentos checkouts Documento  forçando o check-in Documento não foi verificado. Documentos checked out Erro tente novamente realizar o check no documento; %s Erro tentando check-out de documentos;%s Forçando o check-in documentos não Usuário Sim 