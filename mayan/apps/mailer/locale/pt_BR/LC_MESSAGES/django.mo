��          �   %   �      @     A     F  	   T     ^     w     �     �     �  
   �     �     �  !         "     .  #   6     Z     _     |     �  +   �  /   �  2   �  4   +  7   `  �  �     7     =  
   I     T     n     �     �     �     �  $   �  $   �  %   !     G     Z  (   b     �  #   �     �     �  *   �  >     2   E  >   x  L   �                                                                           	       
                                            Body Date and time Date time Document: {{ document }} Email address Email document Email document: %s Email documents: %s Email link Email link for document: %s Email links for documents: %s Link for document: {{ document }} Log entries Mailing Must provide at least one document. Send Send document link via email Send document via email Subject Successfully queued for delivery via email. Template for the document email form body line. Template for the document email form subject line. Template for the document link email form body line. Template for the document link email form subject line. Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-27 14:13-0400
PO-Revision-Date: 2016-03-21 21:07+0000
Last-Translator: Roberto Rosario
Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Corpo data e hora hora, data Documento: {{ document }} Endereço de E-mail Email documento E-mail de documentos: %s E-mail de documentos: %s Email de ligação link de E-mail  para  documento: %s  link de E-mail  para  documento: %s  Link para o documento: {{ document }} As entradas de log Mailing Deve fornecer, pelo menos, um documento. Enviar Enviar link do documento por e-mail Enviar documento por  e-mail Assunto Sucesso da Fila para a entrega via e-mail. Modelo para o documento, linha de corpo formulário de e-mail. Modelo para o documento, linha de assuntos e-mail. Modelo para o documento, linha de corpo formulário de e-mail. Template para a linha de formulário electrónico Assunto link do documento. 